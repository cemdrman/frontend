FROM node:10.15.3

COPY package*.json ./

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory
COPY . .

# build app
RUN npm run build

EXPOSE 8000
CMD [ "npm" ,"run" ,"start" ]